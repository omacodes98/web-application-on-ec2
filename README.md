# Web Application on EC2 Instance (manually)

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Tests](#test)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Created and configured EC2 instance on AWS 

* Install Docker on remote EC2 instance 

* Deploy Docker image from private Docker repository on EC2 instance 

## Technologies Used

* AWS 

* Docker 

* Linux 

## Steps 

Step 1: Start by creating an AWS account 

Step 2: Search EC2 on AWS and launch instance 

Step 3: When you click on launch instance you would have a new page in which you will be shown various things to configure for EC2 instance. The first will be image in which i selected Amazon Linux 

[OS image](/images/01_config_os_image.png)

Step 4: Select appropriate instance type 

[Instance type](/images/02_instance_type.png) 

Step 5: Configure the network setting, you will need to create a security  group in which you will end up configuring the security group rules which is similar to a firewall. In this we will want to open the port that ssh listens on which is port 22. Also for security reasons you will only want a set of peoples IP addresses that are working on this project to have access to this project only. In my case i put only my ip to have access to the ec2 instance 

[Network config 1](/images/03_confiig_network_setting.png)
[Network config 2](/images/04_config_network_setting_1.png)

Step 6: Configure key pair login in which you will create a name for this key pair. When you do it will display a page that has your Access key ID and secrect access key, you want to download this and save it somewhere save in your system.

Step 7: Launch instance

[Instance Dashboard](/images/05_ec2_dashboard.png)

Step 8: Move the private key you got from the key pair step in to the .ssh directory 

     mv Downloads/docker-server.pem .ssh/ 

[Move Private key](/images/06_moving_private_key_to_ssh_directory.png)

Step 9: Chage permission of the file containing the key

     ls -l .ssh/docker-server.pem
     chmod 400 .ssh/docker-server.pem
     ls -l .ssh/docker-server.pem

[Changing permission](/images/07_changing_permission_of_the_file.png)

Step 10: ssh into the ec2 instance 

     ssh -i .ssh/docker-server.pem ec2-user@35.179.90.128

[ssh to ec2 instance](/images/08_ssh_into_server.png)

Step 11: Now we are inside the ec2 instance we need to download docker however before downloading docker make sure you update yum

     sudo yum update 
     sudo yum install docker

Step 12: Start docker

     sudo systemctl start docker

Step 13: Add user to docker group so you could have access to docker commands 

     sudo usermod -aG docker $USER 

[Adding ec2_user to docker group](/images/12_adding_user_to_docker_group.png)

Step 14: Now on your local machine build and push image to dockerhub 

     docker build -t omacodes98/new-node-app:1.0 
     docker push omacodes98/new-node-app:1.0 

[Build and push image](/images/13_building_and_pushing_docker_image_to_docker_hub_repo.png)

Step 15: login into dockerhub on server and pull image 

     docker login
     docker pull omacodes98/new-node-app:1.0

[docker login](/images/14_docker_login_on_server.png)

Step 16: Run application as a docker container 

     docker run -d -p 3000:80 5b1c78a4afbd 

[docker container](/images/16_running_application_as_a_docker_container.png)

Step 17: Open the port that the server host is using to connect to the container port, in my case tis will be port 3000

[Opening port 3000](/images/17_opening_port_3000_in_security_group.png)

Step 18: Test if application UI is working on browser 

[UI on browser](/images/18_application_ui_on_browser.png)


## Installation

Run $ sudo yum install docker 

## Usage 

Run $ node server.js 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/web-application-on-ec2.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review

## Tests

Test were ran using npm test.

## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/web-application-on-ec2

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.